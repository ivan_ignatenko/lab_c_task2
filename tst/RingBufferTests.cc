#include "../src/RingBuffer.h"
#include "../src/RingBuffer.cpp"
#include "gtest/gtest.h"

TEST(constructor,Normal){
    RingBuffer<int> r(2);
    r.push(1);
    r.push(2);
    r.push(3);
    RingBuffer<int>::Iterator_rb it(r);
    int expected = *it;
    EXPECT_EQ(3,r.getSize());
    EXPECT_EQ(1,r.get());
    EXPECT_EQ(1,expected);
}

TEST(iterator,Normal){
    RingBuffer<int> r(2);
    r.push(1);
    r.push(2);
    r.push(3);
    RingBuffer<int>::Iterator_rb it(r);
    int expected = *it;
    EXPECT_EQ(1,expected);
    ++it;
    expected = *it;
    EXPECT_EQ(2,expected);
    ++it;
    expected = *it;
    EXPECT_EQ(3,expected);
}
TEST(iterator1,Normal) {

    RingBuffer<int> r(2);
    EXPECT_TRUE(r.isEmpty());
    r.push(1);
    r.push(2);
    r.push(3);
    int arr[3];
    int i = 0;
   for(auto it =r.begin();it!=r.end() ;++it){
       arr[i] = *it;
       i++;
   }
    EXPECT_EQ(arr[0],1);
    EXPECT_EQ(arr[1],2);
    EXPECT_EQ(arr[2],3);
}
